import React from "react";
import { connect } from "react-redux";
import { decrementBreakSessionAction, incrementBreakSessionAction,
    decrementWorkSessionAction, incrementWorkSessionAction } from "../../actions/actions"

const SessionLength = (props) => {

    const downBreak = () => {
        props.decrementBreakSessionAction();
    }

    const upBreak = () => {
        props.incrementBreakSessionAction();
    }


    const downLength = () => {
        props.decrementWorkSessionAction();
    }

    const upLength = () => {
        props.incrementWorkSessionAction();
    }

    return(
        <div className="d-flex justify-content-center px-5 mx-5">

            <div id="break-label" className="mr-5">
                <h3 className="length-control">Break Length</h3>

                <div className="d-flex flex-row justify-content-center">
                    <button id="break-decrement" className="btn-level" onClick={downBreak}>
                        <i className="fa fa-arrow-down" />
                    </button>
                    <div id="break-length" className="value-break">{props.breakSession}</div>
                    <button id="break-increment" className="btn-level" onClick={upBreak}>
                        <i className="fa fa-arrow-up" />
                    </button>
                </div>

            </div>

            <div id="session-label">
                <h3 className="length-control">Session Length</h3>

                <div className="d-flex flex-row justify-content-center">
                    <button id="session-decrement" className="btn-level" onClick={downLength}>
                        <i className="fa fa-arrow-down" />
                    </button>
                    <div id="session-length" className="value-break">{props.workSession}</div>
                    <button id="session-increment" className="btn-level" onClick={upLength}>
                        <i className="fa fa-arrow-up"/>
                    </button>
                </div>
            </div>
        </div>
    );
}

const mapStateToProps = (state) => {
    return {
        breakSession: state.breakSessionDuration,
        workSession: state.workSessionDuration,
        isClockRunning: state.isClockRunning,
    };
}

const mapDispatchToProps =  { decrementBreakSessionAction, incrementBreakSessionAction,
    decrementWorkSessionAction, incrementWorkSessionAction };

export default connect(mapStateToProps, mapDispatchToProps)(SessionLength);