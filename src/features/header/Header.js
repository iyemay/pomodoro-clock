import React from "react";

function Header() {
    return (
        <div className="d-flex justify-content-center mt-5">
            <strong><h1 className="clock-header">Pomodoro Clock</h1></strong>
        </div>
    );
}

export default Header;