import React from "react";
import { resetAction, startStopClockRunningAction } from "../../actions/actions";
import { connect } from "react-redux";

const SessionTimer = (props) => {

    const playClock = () => {
        props.startStopClockRunningAction();
    }

    const reset = () => {
        return props.resetAction();
    }
    return(
        <div className="d-flex flex-column justify-content-center align-items-center mt-5 timer">

            <div className="d-flex flex-column justify-content-center timer-session py-3 px-5">
                <div id="timer-label" className="d-flex justify-content-center">
                    <p className="label-text">{props.cycle}</p>
                </div>
                <div id="time-left" className="d-flex justify-content-center">
                    <p className="label-number">{props.currentTime}</p>
                </div>
            </div>

            <div className="d-flex justify-content-center mt-3">
                <button id="start_stop" className="btn-level" onClick={playClock}>
                    <i className="fa fa-play" />
                    <i className="fa fa-pause" />
                </button>
                <button id="reset" className="btn-level" onClick={reset}>
                    <i className="fa fa-refresh"/>
                </button>
            </div>
        </div>
    );
}

const mapStateToProps = (state) => {
    return {
        currentTime: state.currentTime,
        cycle: state.cycle,
        workSessionDuration: state.workSessionDuration,
        isClockRunning: state.isClockRunning
    };
}

const mapDispatchToProps = { resetAction, startStopClockRunningAction };

export default connect(mapStateToProps, mapDispatchToProps)(SessionTimer);