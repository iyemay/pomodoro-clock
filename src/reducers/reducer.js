import { DECREMENT_BREAK_SESSION,
    INCREMENT_BREAK_SESSION,
    DECREMENT_WORK_SESSION,
    INCREMENT_WORK_SESSION,
    START_STOP_CLOCK_RUNNING,
    RESET } from "../actions/actions";


const initialState = {
    isClockRunning: false,
    currentTime: "25 : 00",
    cycle: "Session",
    workSessionDuration: 25,
    breakSessionDuration: 5,
};

export default function breakReducer(state = initialState, action) {

    if (action.type === DECREMENT_BREAK_SESSION && state.breakSessionDuration > 1) {
        return {
            ...state,
            breakSessionDuration: state.breakSessionDuration - 1
        }
    } else if (action.type === DECREMENT_BREAK_SESSION && state.breakSessionDuration === 1) {
        return {
            ...state,
            breakSessionDuration: state.breakSessionDuration
        }
    } else if (action.type === INCREMENT_BREAK_SESSION && state.breakSessionDuration < 60) {
        return {
            ...state,
            breakSessionDuration: state.breakSessionDuration + 1
        }
    } else if (action.type === INCREMENT_BREAK_SESSION && state.breakSessionDuration === 60) {
        return {
            ...state,
            breakSessionDuration: state.breakSessionDuration
        }
    } else if (action.type === DECREMENT_WORK_SESSION && state.workSessionDuration > 1) {
        return {
            ...state,
            workSessionDuration: state.workSessionDuration - 1,
            currentTime: state.workSessionDuration - 1
        }
    } else if (action.type === DECREMENT_WORK_SESSION && state.workSessionDuration === 1) {
        return {
            ...state,
            workSessionDuration: state.workSessionDuration,
            currentTime: state.workSessionDuration
        }
    } else if (action.type === INCREMENT_WORK_SESSION && state.workSessionDuration < 60) {
        return {
            ...state,
            workSessionDuration: state.workSessionDuration + 1,
            currentTime: state.workSessionDuration + 1
        }
    } else if (action.type === INCREMENT_WORK_SESSION && state.workSessionDuration === 60) {
        return {
            ...state,
            workSessionDuration: state.workSessionDuration,
            currentTime: state.workSessionDuration
        }
    } else if (action.type === START_STOP_CLOCK_RUNNING) {
        return {
            ...state,
            isClockRunning: true,
            currentTime: state.workSessionDuration - 1
        }
    } else if (action.type === RESET) {
        return initialState;
    } else {
        return initialState;
    }
}