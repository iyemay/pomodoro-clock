import React from 'react';
import './App.scss';
import Header from './features/header/Header.js';
import SessionLength from './features/session-length/SessionLength';
import SessionTimer from "./features/session-timer/SessionTimer";

function App() {
  return (
    <div className="d-flex flex-column mx-auto w-75">
      <Header />
      <SessionLength />
      <SessionTimer />
    </div>
  );
}

export default App;
