export const DECREMENT_BREAK_SESSION = 'DECREMENT_BREAK_SESSION'
export const INCREMENT_BREAK_SESSION = 'INCREMENT_BREAK_SESSION'
export const DECREMENT_WORK_SESSION = 'DECREMENT_WORK_SESSION'
export const INCREMENT_WORK_SESSION = 'INCREMENT_WORK_SESSION'
export const START_STOP_CLOCK_RUNNING = 'START_STOP_CLOCK_RUNNING'
export const RESET = 'RESET'

export function decrementBreakSessionAction() {
    return {
        type: DECREMENT_BREAK_SESSION
    }
}

export function incrementBreakSessionAction() {
    return {
        type: INCREMENT_BREAK_SESSION
    }
}

export function decrementWorkSessionAction() {
    return {
        type: DECREMENT_WORK_SESSION
    }
}

export function incrementWorkSessionAction() {
    return {
        type: INCREMENT_WORK_SESSION
    }
}

export function startStopClockRunningAction() {
    return {
        type: START_STOP_CLOCK_RUNNING
    }
}

export function resetAction() {
    return {
        type: RESET
    }
}
