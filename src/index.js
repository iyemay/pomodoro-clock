import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';
import './index.css';
import '../node_modules/font-awesome/css/font-awesome.min.css';
import App from './App';
import './features/header/Header.scss';
import './features/session-length/SessionLength.scss';
import './features/session-timer/SessionTimer.scss';
import logger from 'redux-logger';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import breakReducer from "./reducers/reducer";


const store = createStore(
    breakReducer,
    applyMiddleware(logger)
);

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

